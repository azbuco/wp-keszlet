<?php

function custom_post_type_wp_keszlet() {
    $labels = array(
        'name' => 'Autó',
        'singular_name' => 'Autó',
        'menu_name' => 'Autók',
        'parent_item_colon' => 'Szülő',
        'all_items' => 'Összes autó',
        'view_item' => 'Autó megtekintése',
        'add_new_item' => 'Új autó hozzáadása',
        'add_new' => 'Új hozzáadása',
        'edit_item' => 'Autó szerkesztése',
        'update_item' => 'Autó módosítása',
        'search_items' => 'Autó keresése',
        'not_found' => 'Nem található',
        'not_found_in_trash' => 'Nem található a lomtárban.'
    );

    $args = array(
        'description' => 'Aktuális eladó autók',
        'labels' => $labels,
        'supports' => array('title', /* 'thumbnail', 'editor', 'excerpt', 'author', 'comments', 'revisions',  'custom-fields', */),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 10,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    register_post_type(WP_KESZLET_SLUG, $args);
}

add_action('init', 'custom_post_type_wp_keszlet', 0);
