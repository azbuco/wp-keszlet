<?php

function wp_keszlet_custom_single_template($single) {
    global $post;

    /* Checks for single template by post type */
    if ($post->post_type == WP_KESZLET_SLUG) {
        if (file_exists(WP_KESZLET_DIR . '/template/single.php')) {
            return WP_KESZLET_DIR . '/template/single.php';
        }
    }

    return $single;
}

add_filter('single_template', 'wp_keszlet_custom_single_template');

function wp_keszlet_custom_archive_template($archive_template) {
    global $post;

    if (is_post_type_archive(WP_KESZLET_SLUG)) {
        if (file_exists(WP_KESZLET_DIR . '/template/archive.php')) {
            return WP_KESZLET_DIR . '/template/archive.php';
        }
    }
    return $archive_template;
}

add_filter('archive_template', 'wp_keszlet_custom_archive_template');
