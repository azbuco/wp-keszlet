<?php

add_action('wp_enqueue_scripts', function() {
    
	if (is_singular(WP_KESZLET_SLUG)) {
		wp_enqueue_style('wp-keszlet', plugin_dir_url(WP_KESZLET_FILE) . '/web/css/wp-keszlet.css', ['bootstrap']);
	}
   
});

