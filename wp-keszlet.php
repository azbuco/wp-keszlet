<?php

/**
 * Plugin Name: WP Készlet
 * Description: Készleten lévő autók megjelenítése
 * Author: Székely Tamás
 * Version: 1.0.10
 */

define('WP_KESZLET_FILE', __FILE__);
define('WP_KESZLET_DIR', __DIR__);
define('WP_KESZLET_SLUG', 'keszlet');

require_once __DIR__ . '/includes/cpt.php';
require_once __DIR__ . '/includes/enqueue.php';
require_once __DIR__ . '/includes/fields.php';
require_once __DIR__ . '/includes/functions.php';


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/azbuco/wp-keszlet',
	__FILE__,
	'wp-keszlet'
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
//$myUpdateChecker->setAuthentication(array(
//	'consumer_key' => '...',
//	'consumer_secret' => '...',
//));

//Optional: Set the branch that contains the stable release.
//$myUpdateChecker->setBranch('stable-branch-name');



