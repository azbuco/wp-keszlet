<?php
cc_debug('Template: ' . __FILE__);
get_header();
?>

<div class="content-area mt-0">

    <?php while (have_posts()) : the_post(); ?> 
        <?php
        $modell = get_field('modell');
        $tipus = get_field('tipus');
        $video = get_field('video');
        $lista_ar = get_field('lista_ar');
        if (is_numeric($lista_ar)) {
            $lista_ar = number_format($lista_ar, 0, ',', ' ') . 'Ft';
        }
        $kedvezmenyes_ar = get_field('kedvezmenyes_ar');
        if (is_numeric($kedvezmenyes_ar)) {
            $kedvezmenyes_ar = number_format($kedvezmenyes_ar, 0, ',', ' ') . 'Ft';
        }
        $motor = get_field('motor');
        $ccm = get_field('ccm');
        $teljesitmeny = get_field('teljesitmeny');
        $hajtas = get_field('hajtas');

        $kiemelt_felszereltseg = get_field('kiemelt_felszereltseg');
        $teljes_felszereltseg = get_field('teljes_felszereltseg');

        $galeria = get_field('galeria');
        ?>

        <div class="video-intro w-100">
            <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" class="">
                <source src="<?= $video['url'] ?>" type="video/mp4">
            </video>
            <div class="info-container">
                <div class="info-box">
                    <h1 class="title mb-3"><?= $tipus ?><br /><small><?= $modell ?></small></h1>
                    <table class="table">
                        <tr><th>Listaár</th><td><?= $lista_ar ?></td></tr>
                        <tr class="inverse"><th>Kedvezményes vételár</th><td class="font-weight-bold"><?= $kedvezmenyes_ar ?></td></tr>
                        <tr><th>Motor</th><td class="font-weight-bold"><?= $motor ?></td></tr>
                        <tr><th>Teljesítmény</th><td class="font-weight-bold"><?= $teljesitmeny ?></td></tr>
                        <tr><th>Hajtás</th><td class="font-weight-bold"><?= $hajtas ?></td></tr>
                    </table>
                    <div class="text-center">
                        <a href="#foglalas" class="btn btn-primary">Érdekel az autó</a>
                    </div>
                </div>
                <nav>
                    <a class="info-link" href="#felszereltseg">
                        <div class="d-flex align-items-center">
                            <div class="info-icon">
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                <path fill="currentColor" d="M4 13C2.89 13 2 13.89 2 15V19C2 20.11 2.89 21 4 21H8C9.11 21 10 20.11 10 19V15C10 13.89 9.11 13 8 13M8.2 14.5L9.26 15.55L5.27 19.5L2.74 16.95L3.81 15.9L5.28 17.39M4 3C2.89 3 2 3.89 2 5V9C2 10.11 2.89 11 4 11H8C9.11 11 10 10.11 10 9V5C10 3.89 9.11 3 8 3M4 5H8V9H4M12 5H22V7H12M12 19V17H22V19M12 11H22V13H12Z" />
                                </svg>
                            </div>
                            <div class="info-label title">Felszereltség</div>
                        </div>
                    </a>
                    <a class="info-link" href="#galeria">
                        <div class="d-flex align-items-center">
                            <div class="info-icon">
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                <path fill="currentColor" d="M21,17H7V3H21M21,1H7A2,2 0 0,0 5,3V17A2,2 0 0,0 7,19H21A2,2 0 0,0 23,17V3A2,2 0 0,0 21,1M3,5H1V21A2,2 0 0,0 3,23H19V21H3M15.96,10.29L13.21,13.83L11.25,11.47L8.5,15H19.5L15.96,10.29Z" />
                                </svg>
                            </div>
                            <div class="info-label title">Galéria</div>
                        </div>
                    </a>
                    <!--
                    <a class="info-link" href="#finanszirozas">
                        <div class="d-flex align-items-center">
                            <div class="info-icon">
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                <path fill="currentColor" d="M20,8H4V6H20M20,18H4V12H20M20,4H4C2.89,4 2,4.89 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V6C22,4.89 21.1,4 20,4Z" />
                                </svg>
                            </div>
                            <div class="info-label title">Finanszírozás</div>
                        </div>
                    </a>
                    -->
                </nav>
            </div>

            <div class="info-notice">
                Ezen az oldalon az összes média tartalom a fenti járművet valós állapotában mutatja.
            </div>        
        </div>

        <div class="container">

            <!-- Felszereltseg -->
            <div id="felszereltseg">
                <h2 class="display-4 my-5 text-center">
                    Felszereltség
                </h2>
                <div class="specifications lead title">
                    <ul>
                        <?php foreach (explode("\n", $kiemelt_felszereltseg) as $item): ?>
                            <li><?= $item ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="text-center">
                    <a class="btn btn-primary btn-sm my-5" data-toggle="collapse" data-ignore-jump="true" href="#specifications-more" role="button" aria-expanded="false" aria-controls="specifications-more">
                        Teljes felszereltség
                    </a>
                </div>
                <div class="specifications-more collapse mb-5" id="specifications-more">
                    <?= $teljes_felszereltseg ?>
                </div>
            </div>
            <!-- /Felszereltseg -->

            <!-- Galéria -->
            <div id="galeria">
                <h2 class="display-4 my-5 text-center" id="galeria">
                    Nézze meg következő autóját közelebbről
                </h2>
                <div class="showroom row">
                    <?php for ($i = 0; $i < 12; $i++): $image = $galeria[$i]; ?>
                        <div class="col-6 col-md-4 col-lg-3">
                            <figure>
                                <a href="#" data-featherlight="<?= $image['sizes']['post-thumbnail'] ?>">
                                    <img src="<?= $image['sizes']['medium'] ?>" />
                                </a>
                            </figure>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="text-center">
                    <a class="btn btn-primary btn-sm mt-3 mb-5" data-toggle="collapse" data-ignore-jump="true" href="#showroom-more" role="button" aria-expanded="false" aria-controls="showroom-more">
                        Még több kép
                    </a>
                </div>
                <?php if (count($galeria) > 11): ?>
                    <div class="showroom row collapse mb-5" id="showroom-more">
                        <?php for ($i = 12; $i < count($galeria); $i++): $image = $galeria[$i]; ?>
                            <div class="col-6 col-md-4 col-lg-3">
                                <figure>
                                    <a href="#" data-featherlight="<?= $image['sizes']['post-thumbnail'] ?>">
                                        <img src="<?= $image['sizes']['medium'] ?>" />
                                    </a>
                                </figure>
                            </div>
                        <?php endfor; ?>
                    </div>
                <?php endif; ?>
            </div>
            <!-- /Galéria -->
        </div>


        <!-- Finanszírozási ajánlat -->
        <!--
        <div class="bg-image" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(<?= $galeria[0]['sizes']['post-thumbnail'] ?>);">
            <div class="container">
                <div id="finanszirozas">

                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <h2 class="display-4 my-5 text-center" id="galeria">
                                Finanszírozási<br />ajánlat
                            </h2>
                        </div>

                        <div class="col-md-6">
                            <div class="finance">
                                <h5 class="title">Kedvezményes vételár</h5>
                                <table class="table mb-3">
                                    <tr><th>Listaár</th><td>7 930 000 Ft</td></tr>
                                    <tr><th>Kiemelt kedvezmény</th><td>300 000 Ft</td></tr>
                                    <tr><th>EASY kedvezmény</th><td>100 000 Ft</td></tr>
                                    <tr><th>EASY programban 3+2 Extracare garanciával</th><td>150 000 Ft</td></tr>
                                    <tr class="inverse"><th><b>Online megrendelés kedvezmény</b></th><td><b>100 000 Ft</b></td></tr>
                                    <tr><th>Teljes árelőny:</th><td>650 000 Ft</td></tr>

                                    <tr><th>Kedvezményes vételár:</th><td>7 430 000 Ft</td></tr>
                                </table>

                                <h5 class="title">Finanszírozás EASY Programban</h5>
                                <table class="table">
                                    <tr><th>Bruttó ár:</th><td>7 530 000 Ft</td></tr>
                                    <tr><th>Kezdő részlet:</th><td>1 506 000 Ft</td></tr>
                                    <tr><th>Finanszírozott összeg:</th><td>6 024 000 Ft</td></tr>
                                    <tr><th>Maradványérték:</th><td>1 581 300 Ft</td></tr>
                                    <tr><th>Futamidő:</th><td>48 hónap</td></tr>
                                    <tr><th>THM</th><td>4,9%</td></tr>
                                    <tr><th>Törlesztő részlet</th><td>110 225 Ft/hó</td></tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /finanszírozási ajánlat -->

        <!-- Foglalás -->
        <div class="bg-image pt-5" style="background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(<?= $galeria[0]['sizes']['post-thumbnail'] ?>);">  
            <div class="container">

                <div id="foglalas">

                    <h2 class="display-4 mt-5 mb-3 text-center" id="foglalas">
                        Foglalás
                    </h2>
                    <h3 class="lead text-center mb-5">Amennyiben felkeltettük érdeklődését, kérjük jelezze vételi szándékát az alábbi űrlapon:</h3>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-5">
                                <h5 class="title">A vásárlás menete</h5>
                                <p>Kérjük töltse ki a következő űrlapot.</p>
                                <p>Az űrlap feldolgozását követően értékesítő munkatársunk megkeresi Önt, és egyeztet a részletekről.</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-5">
                                <h5 class="title">Vásárlási szándék jelzése</h5>
                                <?= do_shortcode(WP_KESZLET_CONTACT_FORM) ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /foglalás -->

    <?php endwhile; ?>

</div>

<?php
get_footer();
