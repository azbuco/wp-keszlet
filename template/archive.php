<?php
cc_debug('Template: ' . __FILE__);
get_header();
?>



<div class="content-area">
    <main>
        <h1 class="text-center">Kiemelt készlet ajánlatunk</h1>
        <div class="lead text-center mb-5">
            <p>
                Tekintse meg készleten lévő, azonnal elvihető ajánlatunk, és foglalja le kedvenc autóját akár most!
            </p>
        </div>

        <?php if (have_posts()): ?> 
            <div class="kiemelt-keszlet">
                <?php
                $index = 0;
                while (have_posts()): the_post();
                    $index++;
                    ?>
                    <?php
                    $modell = get_field('modell');
                    $tipus = get_field('tipus');
                    $video = get_field('video');
                    $lista_ar = get_field('lista_ar');
                    if (is_numeric($lista_ar)) {
                        $lista_ar = number_format($lista_ar, 0, ',', ' ') . 'Ft';
                    }
                    $kedvezmenyes_ar = get_field('kedvezmenyes_ar');
                    if (is_numeric($kedvezmenyes_ar)) {
                        $kedvezmenyes_ar = number_format($kedvezmenyes_ar, 0, ',', ' ') . 'Ft';
                    }
                    $motor = get_field('motor');
                    $ccm = get_field('ccm');
                    $teljesitmeny = get_field('teljesitmeny');
                    $hajtas = get_field('hajtas');

                    $kiemelt_felszereltseg = get_field('kiemelt_felszereltseg');
                    $teljes_felszereltseg = get_field('teljes_felszereltseg');

                    $galeria = get_field('galeria');
                    ?>

                    <div class="py-5" <?= $index % 2 === 1 ? 'style="background-color: #f1f1f1;"' : '' ?>>
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <video playsinline="playsinline" muted="muted" controls="controls" poster="<?= $galeria[0]['sizes']['post-thumbnail'] ?>" class="w-100">
                                        <source src="<?= $video['url'] ?>" type="video/mp4">
                                    </video>
                                </div>
                                <div class="col-md-4 <?= $index % 2 === 0 ? 'order-first' : 'order-last' ?>">
                                    <h2 class="title mb-3"><?= $tipus ?><br /><small><?= $modell ?></small></h2>
                                    <table class="table">
                                        <tr><th>Listaár</th><td class="text-right"><?= $lista_ar ?></td></tr>
                                        <tr class="inverse"><th>Kedvezményes vételár</th><td class="font-weight-bold text-right"><?= $kedvezmenyes_ar ?></td></tr>
                                        <tr><th>Motor</th><td class="text-right"><?= $motor ?></td></tr>
                                        <tr><th>Teljesítmény</th><td class="text-right"><?= $teljesitmeny ?></td></tr>
                                        <tr><th>Hajtás</th><td class="text-right"><?= $hajtas ?></td></tr>
                                    </table>
                                    <div class="text-center">
                                        <a href="<?= the_permalink() ?>" class="btn btn-primary">Érdekel az autó</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <?php endwhile; ?>
            </div>
        <?php else: ?>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title">
                            Nincs megjeleníthető bejegyzés
                        </h1>
                        <p>
                            Sajnáljuk, de a kategóriában nincs még bejegyzés. Nézzen vissza kicsit később.
                        </p>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </main>
</div>


<?php
get_footer();
